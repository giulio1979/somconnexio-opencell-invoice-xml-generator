package org.meveo.service.script;

import java.io.File;
import java.lang.Double;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import java.util.concurrent.TimeUnit;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.meveo.admin.exception.BusinessException;
import org.meveo.commons.utils.ParamBeanFactory;
import org.meveo.commons.utils.ParamBean;
import org.meveo.commons.utils.PersistenceUtils;
import org.meveo.model.crm.Provider;
import org.meveo.model.admin.Seller;
import org.meveo.model.ICustomFieldEntity;
import org.meveo.model.admin.User;
import org.meveo.model.billing.BankCoordinates;
import org.meveo.model.billing.Invoice;
import org.meveo.model.billing.InvoiceAgregate;
import org.meveo.model.billing.RatedTransaction;
import org.meveo.model.billing.ServiceInstance;
import org.meveo.model.billing.Subscription;
import org.meveo.model.billing.WalletOperation;
import org.meveo.model.payments.CustomerAccount;
import org.meveo.model.payments.PaymentMethod;
import org.meveo.model.payments.PaymentMethodEnum;
import org.meveo.model.payments.DDPaymentMethod;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.billing.impl.RatedTransactionService;
import org.meveo.service.billing.impl.WalletOperationService;
import org.meveo.service.billing.impl.XMLInvoiceCreator;
import org.meveo.service.crm.impl.CustomFieldInstanceService;
import org.meveo.service.crm.impl.ProviderService;
import org.meveo.service.script.Script;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

/**
 *
 * @author anasseh
 * @author Coopdevs
 *
 *         Generate custom Xml Invoice needed to fill the Som Connexio invoice
 *
 *         Execute Context Variable :
 *         <li>CONTEXT_ENTITY : the invoice
 *         <li>isVirtual : invoicing mode
 *         <li>XMLInvoiceCreator : XMLInvoiceCreator std service
 *
 * Object: [SCN]
 *   + Add IBAN from CustomerAccount
 *   + Add MSIDSN to every consumption lines
 *   + Add Service Description to every consumption lines
 *   + Add Subscriptions List to invoice XML
 *
 */
public class InvoiceXmlScript extends org.meveo.service.script.Script {

  private static final Logger log = LoggerFactory.getLogger(InvoiceXmlScript.class);

  private CustomFieldInstanceService cfiService = (CustomFieldInstanceService) getServiceInterface("CustomFieldInstanceService");
  private Provider provider = ((ProviderService)getServiceInterface("ProviderService")).getProvider();

  private RatedTransactionService ratedTransactionService = (RatedTransactionService) getServiceInterface("RatedTransactionService");
  private WalletOperationService walletOperationService = (WalletOperationService) getServiceInterface("WalletOperationService");

  private List<RatedTransaction> ratedTransactions = new ArrayList<>();

  private List<WalletOperation> subscriptionWOList = new ArrayList<>();
  private List<WalletOperation> consumptionsWOList = new ArrayList<>();
  private List<WalletOperation> oneshotWOList = new ArrayList<>();

  private String lang = "";

  private ParamBean paramBean = ParamBean.getInstance();
  private static String DEFAULT_DATE_PATTERN = "dd/MM/yyyy";
  private static Integer DEFAULT_SCALE_VALUE = 4;

  @Override
  public void execute(Map<String, Object> methodContext) throws BusinessException {
    String invoice_number = "";
    Date startDate = new Date();
    log.debug("EXECUTE  methodContext {}", methodContext);
    try{
      XMLInvoiceCreator xmlInvoiceCreator = (XMLInvoiceCreator) methodContext.get("XMLInvoiceCreator");
      Invoice invoice = (Invoice) methodContext.get(Script.CONTEXT_ENTITY);
      lang = invoice.getBillingAccount().getTradingLanguage().getLanguageCode();

      CustomerAccount customerAccount = invoice.getBillingAccount().getCustomerAccount();

      Boolean isVirtual = (Boolean) methodContext.get("isVirtual");

      Document document = xmlInvoiceCreator.createDocument(invoice,isVirtual.booleanValue());

      // Add Tag IBAN to Invoice Element
      addIban(customerAccount, document);

      // Modify the detail lines
      modifyConsumptionsData(document, invoice);

      ratedTransactions = ratedTransactionService.getRatedTransactionsByInvoice(invoice, false);

      // InvoiceLines List
      addInvoiceLinesToDocument(document, invoice);

      // Only group data consumptions if invoice is COM type.
      if (getInvoiceType(invoice).equals("COM")) {
        groupDataConsumptionsByDate(document);
      }

      // Remove the Subscriptions categories
      removeSubscriptionsCategories(document);

      File file = xmlInvoiceCreator.createFile(document,invoice);
      methodContext.put(Script.RESULT_VALUE, file);

      printScriptTime(startDate, invoice);
    }catch(Exception e){
      throw new BusinessException(e.getMessage());
    }
  }

  private void addInvoiceLinesToDocument(Document document, Invoice invoice) {
      // InvoiceLines List
      Element invoiceElement = (Element)document.getElementsByTagName("invoice").item(0);
      String invoiceType = getInvoiceType(invoice);
      switch (invoiceType) {
        case "COM": // Comercial invoice
          addInvoiceLinesForCommercial(invoice, invoiceElement, document);
          break;
        case "ADJ": // Adjustment invoice
          addInvoiceLinesForAssociate(invoice, invoiceElement, document);
          break;
        case "AGR": // Aggregate invoice
          addInvoiceLinesForAssociate(invoice, invoiceElement, document);
          break;
      }
  }

  private static void printScriptTime(Date startDate, Invoice invoice) {
    Date endDate = new Date();
    log.warn("XML generation of {} runs in {} ms", invoice.getInvoiceNumber(), endDate.getTime() - startDate.getTime());
  }

  private String getInvoiceType(Invoice invoice) {
    if (invoice.getLinkedInvoices().size() > 0 && invoice.getInvoiceType().getCode().equals("COM")){
      return "AGR";
    }
    return invoice.getInvoiceType().getCode();
  }

 /**
  * Remove the Subscriptions from the Categories entry inside the details in the XML document.
  * We need this utility to avoid print this sections in the PDF with Jasper.
  * TODO: We are using the filter in the dataset definition: /addressbook/category[@name = "work"]/person
  * TODO: We can remove this piece of code if works fine in the test of Monday 9 of September
  *
  * @param document XML invoice document
  */
  private void removeSubscriptionsCategories(Document document) {
    try{
      List<Element> subscriptionCategories = getSubscriptionsFromDocument(document);
      Element sub = subscriptionCategories.get(0);
      Node subCategory = sub.getParentNode();
      Node subCategories = subCategory.getParentNode();
      Node category = subCategories.getParentNode();
      Node categories = category.getParentNode();
      categories.removeChild(category);
    } catch (Exception e) {
      log.warn("Exception removing the Subscriptions of the detailed invoice information...");
    }
  }

 /**
  * Return the list of subscriptions associated to this invoice.
  * Get the BillingAccount from the invoice, after the UserAccounts and then the subscriptions of the first UserAccount
  * in our data model can not exist more than one UserAccount by Billing Account
  *
  * @param invoice        The invoice instance
  * @return subscriotions List of subscription instances
  */
  private List<Subscription> getSubscriptions(Invoice invoice) {
    List<Subscription> subscriptions = null;
    try{
      subscriptions = invoice.getBillingAccount().getUsersAccounts().get(0).getSubscriptions();
    } catch (Exception e) {
      log.warn("Exception getting the subscriptions related with the invoice {}", invoice.getInvoiceNumber());
    }
    return subscriptions;
  }

 /**
  * Return the first WalletOperation instance associated to the RatedTransaction if exists, else
  * return null
  *
  * @param trans   The rated transaction instance
  * @return wallet The wallet operation instance
  */
  private WalletOperation getWalletOperation(RatedTransaction trans) {
    List<WalletOperation> woList = walletOperationService.listByRatedTransactionId(trans.getId());
    if ( woList.isEmpty() ) {
      return null;
    }
    return woList.get(0);
  }

  private String convertDateToString(Date date) {
    String invoiceDateFormat = paramBean.getProperty("invoice.dateFormat", DEFAULT_DATE_PATTERN);
    return DateUtils.formatDateWithPattern(date, invoiceDateFormat);
  }

  private String round(BigDecimal amount, Integer scale) {
    if (amount == null) {
      amount = BigDecimal.ZERO;
    }
    if (scale == null) {
      scale = 2;
    }
    amount = amount.setScale(scale, RoundingMode.HALF_UP);

    return amount.toPlainString().replaceAll("(\\d+)\\.(\\d+)", "$1,$2");
  }

  private Element createInvoiceLine(Document doc, String msidsn, String description, String startDate, String endDate, String amount, String subCategory, String tax, String order){
      Element line = doc.createElement("line");
      line.setAttribute("msidsn", msidsn);
      line.setAttribute("description", description);
      line.setAttribute("startDate", startDate);
      line.setAttribute("endDate", endDate);
      line.setAttribute("amount", amount);
      line.setAttribute("subCategory", subCategory);
      line.setAttribute("taxes", tax);
      line.setAttribute("order", order);
      return line;
  }
  private void addAssociateLinesToInvoiceLinesList(Invoice invoice, Element invoiceLines, Document doc) {
  }

  private void addSubscriptionLinesToInvoiceLinesList(Invoice invoice, Element invoiceLines, Document doc) {
    for (WalletOperation wo : subscriptionWOList) {
      Element subscriptionDetails = createInvoiceLine(doc,
        wo.getSubscription().getDescription(),
        getServiceDescriptionTranslated(wo.getChargeInstance().getServiceInstance()),
        convertDateToString(wo.getStartDate()),
        convertDateToString(wo.getEndDate()),
        round(wo.getRatedTransaction().getAmountWithoutTax(), DEFAULT_SCALE_VALUE),
        wo.getDescription(),
        wo.getTax().getDescriptionI18n().get(lang),
        "0");
      invoiceLines.appendChild(subscriptionDetails);
    }
  }


  private HashMap<String,HashMap> groupConsumptionsByInvoiceLines() {
    HashMap<String,HashMap> consumptions = new HashMap<String,HashMap>();
    for (WalletOperation wo : consumptionsWOList) {
      String msidsn = wo.getSubscription().getDescription();
      String description = wo.getDescription();
      if (consumptions.containsKey(msidsn)) {
        // Check if description exists
        HashMap<String,ConsumptionInvoiceLine> consumptionsByMSIDSN = consumptions.get(msidsn);
        if (consumptionsByMSIDSN.containsKey(description)) {
          // Sum the amount to the dict
          ConsumptionInvoiceLine consumptionInvoiceLine = consumptionsByMSIDSN.get(description);
          consumptionInvoiceLine.addAmount(wo.getRatedTransaction().getAmountWithoutTax());
        } else {
          consumptionsByMSIDSN.put(description, createConsumptionAmount(wo));
        }
      } else {
        HashMap<String,ConsumptionInvoiceLine> consumptionsByMSIDSN = new HashMap<String,ConsumptionInvoiceLine>();
        consumptionsByMSIDSN.put(description, createConsumptionAmount(wo));
        consumptions.put(msidsn, consumptionsByMSIDSN);
      }
    }
    return consumptions;
  }

  private ConsumptionInvoiceLine createConsumptionAmount(WalletOperation wo) {
    ConsumptionInvoiceLine consumptionInvoiceLine = new ConsumptionInvoiceLine();
    consumptionInvoiceLine.addAmount(wo.getRatedTransaction().getAmountWithoutTax());
    consumptionInvoiceLine.setTax(wo.getTax().getDescriptionI18n().get(lang));
    return consumptionInvoiceLine;
  }

  private void addConsumptionLinesToInvoiceLinesList(Element invoiceLines, Document doc) {
    HashMap<String,HashMap> consumptionsAsInvoiceLines = groupConsumptionsByInvoiceLines();
    for (String msidsn : consumptionsAsInvoiceLines.keySet()) {
      HashMap<String,ConsumptionInvoiceLine> consumptionsAsInvoiceLinesByMsidsn = consumptionsAsInvoiceLines.get(msidsn);
      for (String description : consumptionsAsInvoiceLinesByMsidsn.keySet()) {
        ConsumptionInvoiceLine consumptionInvoiceLine = consumptionsAsInvoiceLinesByMsidsn.get(description);
        Element consumptionDetails = createInvoiceLine(doc,
          msidsn,
          description,
          "",
          "",
          round(consumptionInvoiceLine.getAmount(), DEFAULT_SCALE_VALUE),
          description,
          consumptionInvoiceLine.getTax(),
          "2"
        );
        invoiceLines.appendChild(consumptionDetails);
      }
    }
  }

  private void addOneShotLinesToInvoiceLinesList(Element invoiceLines, Document doc) {
    for (WalletOperation wo : oneshotWOList) {
      Element oneShotDetails = createInvoiceLine(doc,
        wo.getSubscription().getDescription(),
        wo.getDescription(),
        convertDateToString(wo.getOperationDate()),
        convertDateToString(wo.getEndDate()),
        round(wo.getRatedTransaction().getAmountWithoutTax(), DEFAULT_SCALE_VALUE),
        wo.getDescription(),
        wo.getTax().getDescriptionI18n().get(lang),
        "1");
      invoiceLines.appendChild(oneShotDetails);
    }
  }

  private void getWalletOperationList() {
    for (RatedTransaction trans : ratedTransactions) {
      WalletOperation wo = getWalletOperation(trans);
      if (wo == null) {
        continue;
      }
      if (wo.getInvoiceSubCategory().getCode().contains("SUBSCRIPTION")) {
        subscriptionWOList.add(wo);
        continue;
      }
      if (wo.getInvoiceSubCategory().getCode().contains("CONSUMPTION") && wo.getRatedTransaction().getAmountWithoutTax().compareTo(new BigDecimal("0")) > 0) {
        consumptionsWOList.add(wo);
        continue;
      }
      if (wo.getInvoiceSubCategory().getCode().contains("OSO")) {
        oneshotWOList.add(wo);
        continue;
      }
    }
  }

 /**
  * Add service description (translated) to the consumption data,
  * selecting it from the subscription object.
  *
  * @param  consumption   element with the consumption data to add the service tag and value
  * @param  subscriptions list of subscriptions to search the ones that match with the consumption subscription
  * @return  Void
  */
  private void addServiceToConsumption(Element consumption, List<Subscription> subscriptions) {
    consumption.setAttribute("service", extractServiceFromSubscriptionAndEDR(consumption, subscriptions));
  }

 /**
  * Add msidsn to the consumption data, The msidsn is the line associated to the subscription.
  * We extract it from the EDR information
  *
  * @param  consumption   element with the consumption data to add the msidsn tag and value
  * @return  Void
  */
  private void addMsidsnToConsumption(Element consumption) {
    Element edr = (Element) consumption.getElementsByTagName("edr").item(0);
    String msidsn = edr.getAttribute("accessCode");
    consumption.setAttribute("msidsn", msidsn);
  }

 /**
  * Create and return the HashMap with the data of the data consumption.
  *
  * @param consumption           Element of the invoice document with the consumption data
  * @return dataConsumptionByDay HashMap with the consumption relevant information
  */
  private HashMap<String, Object> createDataConsumptionByDay(Element consumption) {
    String to = "";
    String msidsn = consumption.getAttribute("msidsn");
    String service = consumption.getAttribute("service");
    String param3 = consumption.getAttribute("param3");
    String param2 = consumption.getAttribute("param2");
    Double quantity = Double.valueOf(consumption.getElementsByTagName("quantity").item(0).getTextContent());
    Double amount = Double.valueOf(consumption.getElementsByTagName("amountWithoutTax").item(0).getTextContent());

    HashMap<String,Object> dateConsumptionGroup = new HashMap<>();
    dateConsumptionGroup.put("to", to);
    dateConsumptionGroup.put("msidsn", msidsn);
    dateConsumptionGroup.put("service", service);
    dateConsumptionGroup.put("param2", param2);
    dateConsumptionGroup.put("param3", param3);
    dateConsumptionGroup.put("quantity", quantity);
    dateConsumptionGroup.put("amount", amount);

    return dateConsumptionGroup;
  }

 /**
  * Get the map with the data consumptions groupped by day and update the information with the consumption
  * record passed as an argument.
  *
  * @param dataConsumptions Map with the data consumptions groupped by date.
  * @param consumption      Element of the invoice document with the consumption data.
  */
  private void updateDataConsumptionByDay(HashMap<String,HashMap> dataConsumptionsByDay, Element consumption) {
    String date = consumption.getElementsByTagName("usageDate").item(0).getTextContent();
    if (dataConsumptionsByDay == null) {
      return;
    }
    if (dataConsumptionsByDay.containsKey(date)) {
      HashMap<String,Object> dateConsumptionGroup = dataConsumptionsByDay.get(date);
      Double quantity = Double.valueOf(consumption.getElementsByTagName("quantity").item(0).getTextContent());
      Double amount = Double.valueOf(consumption.getElementsByTagName("amountWithoutTax").item(0).getTextContent());
      dateConsumptionGroup.put("quantity", Double.sum((Double) dateConsumptionGroup.get("quantity"), quantity));
      dateConsumptionGroup.put("amount", Double.sum((Double) dateConsumptionGroup.get("amount"), amount));
    } else {
      dataConsumptionsByDay.put(date, createDataConsumptionByDay(consumption));
    }
  }

 /**
  *
  * @param document      The document generated with the invoice data.
  * @return dataConsumptionsGrouped HashMap with the structure spected to group the data consumptions
  */
  private HashMap<String,HashMap> createConsumptionsGroups(Document document) {
    HashMap<String,HashMap> dataConsumptionsGrouped = new HashMap<String,HashMap>();
    List<String> subCategories = getSubCategoriesCodes(document);
    List<String> msidsns = getMsidsns(document);

    createSubCategoriesGroups(dataConsumptionsGrouped, subCategories);
    for (String subCategory : dataConsumptionsGrouped.keySet()) {
      createMsidsnGroups(((HashMap<String,HashMap>) dataConsumptionsGrouped.get(subCategory)), msidsns);
    }
    return dataConsumptionsGrouped;
  }

  private List<String> getMsidsns(Document document) {
    List<String> msidsns = new ArrayList<String>();
    Element detail = (Element) document.getElementsByTagName("detail").item(0);
    NodeList subscriptions = (NodeList) detail.getElementsByTagName("subscription");
    int lenghtSubscriptions = subscriptions.getLength();
    for (int x=0; x < lenghtSubscriptions; x++) {
      msidsns.add(((Element) subscriptions.item(x)).getAttribute("description"));
    }
    return msidsns;
  }

  private List<Element> getSubCategories(Document document) {
    Element detail = (Element) document.getElementsByTagName("detail").item(0);
    int totalSubCategories = getSubCategoriesLength(detail);
    List<Element> subCategories = new ArrayList<Element>();
    for (int i=0; i < totalSubCategories; i++) {
      subCategories.add((Element) detail.getElementsByTagName("subCategory").item(i));
    }
    return subCategories;
  }

  private List<String> getSubCategoriesCodes(Document document) {
    List<String> subCategories = new ArrayList<String>();
    Element detail = (Element) document.getElementsByTagName("detail").item(0);
    int totalSubCategories = getSubCategoriesLength(detail);
    for (int i=0; i < totalSubCategories; i++) {
      subCategories.add(((Element) detail.getElementsByTagName("subCategory").item(i)).getAttribute("code"));
    }
    return subCategories;
  }

  private void createSubCategoriesGroups(HashMap<String, HashMap> dataConsumptionsGrouped, List<String> subCategories) {
    for (String subCategory : subCategories) {
      dataConsumptionsGrouped.put(subCategory, new HashMap<String, HashMap>());
    }
  }
  private void createMsidsnGroups(HashMap<String, HashMap> subCategoryGroup, List<String> msidsns) {
    for (String msidsn : msidsns) {
      subCategoryGroup.put(msidsn, new HashMap<String, HashMap>());
    }
  }

 /**
  * Group the data consumptions by date to print grouped in the invoice.
  * Remove the old entries of data consumptions and add the new records with the amount and the quantity
  * attributes sumed by date.
  *
  * @param document      The document generated with the invoice data.
  */
  private void groupDataConsumptionsByDate(Document document) {
    HashMap<String,HashMap> dataConsumptionsGrouped = createConsumptionsGroups(document);
    List<Element> dataConsumptions = getDataConsumptionsFromDocument(document);

    for (Element consumption : dataConsumptions) {
      String subCategoryCode = ((Element) consumption.getParentNode()).getAttribute("code");
      String msidsn = consumption.getAttribute("msidsn");
      HashMap<String, HashMap> subCategoryGroups = (HashMap<String, HashMap>) dataConsumptionsGrouped.get(subCategoryCode);
      HashMap<String, HashMap> msidsnGroups = (HashMap<String, HashMap>) subCategoryGroups.get(msidsn);
      updateDataConsumptionByDay(msidsnGroups, consumption);
      // I can't find a revoceAllChilds method in the API: https://docs.oracle.com/javase/8/docs/api/org/w3c/dom/Element.html
      consumption.getParentNode().removeChild(consumption);
    }
    List<Element> subCategories = getSubCategories(document);
    for (Element subCategory : subCategories) {
      String subCategoryCode = subCategory.getAttribute("code");
      HashMap<String, HashMap> subCategoryGroup = (HashMap<String, HashMap>) dataConsumptionsGrouped.get(subCategoryCode);
      fillSubCategory(subCategory, subCategoryGroup, document);
    }
  }

  private void fillSubCategory(Element subCategory, HashMap<String, HashMap> msidsnsMap, Document document) {
    for (String msidsn : msidsnsMap.keySet()) {
      for (String date : ((HashMap<String, HashMap>) msidsnsMap.get(msidsn)).keySet()) {
        subCategory.appendChild(constructDataConsumptionLine(((HashMap<String, Object>) msidsnsMap.get(msidsn).get(date)), date, document));
      }
    }
  }

 /**
  * Modify the consumptions entries of the invoice document:
  * * Add Service description
  * * Add msidsn
  *
  * @param subscrioptions List of subscriptions objects active in this invoice.
  * @param document The document generated with the invoice data.
  */
  private void modifyConsumptionsData(Document document, Invoice invoice) {
    List<Subscription> subscriptions = getSubscriptions(invoice);
    List<Element> consumptions = getConsumptionsFromDocument(document);
    for (Element consumption : consumptions) {
      addServiceToConsumption(consumption, subscriptions);
      addMsidsnToConsumption(consumption);
    }
  }

  private Element constructDataConsumptionLine(HashMap<String, Object> rawDataConsumption, String date, Document document) {
      Element dataConsumptionElement = document.createElement("line");

      Element usageDate = document.createElement("usageDate");
      usageDate.setTextContent(date);
      dataConsumptionElement.appendChild(usageDate);

      Element quantity = document.createElement("quantity");
      quantity.setTextContent(String.valueOf(rawDataConsumption.get("quantity")));
      dataConsumptionElement.appendChild(quantity);

      Element amountWithoutTax = document.createElement("amountWithoutTax");
      amountWithoutTax.setTextContent(String.valueOf(rawDataConsumption.get("amount")));
      dataConsumptionElement.appendChild(amountWithoutTax);

      Element edr = document.createElement("edr");
      edr.setAttribute("accessCode", (String) rawDataConsumption.get("msidsn"));
      edr.setAttribute("subscription", (String) rawDataConsumption.get("msidsn"));
      edr.setAttribute("eventDate", date.replace("/", "-"));
      edr.setAttribute("parameter2", (String) rawDataConsumption.get("param2"));
      edr.setAttribute("parameter3", (String) rawDataConsumption.get("to"));
      edr.setAttribute("parameter5", date.replace("/", "-"));
      edr.setAttribute("parameter6", "00:00:00");
      dataConsumptionElement.appendChild(edr);

      dataConsumptionElement.setAttribute("service", (String) rawDataConsumption.get("service"));
      dataConsumptionElement.setAttribute("msidsn", (String) rawDataConsumption.get("msidsn"));
      dataConsumptionElement.setAttribute("param2", (String) rawDataConsumption.get("param2"));


      return dataConsumptionElement;
  }

 /**
  * Return the service description in the language passed as argument.
  * In the version 9.0 of OC, the Service description will be translatable, but now it isn't.
  * We use a CustomField to save the label in catalan and in spanish and use the language passed
  * as argument to return the correct label located.
  *
  * @param  subscription Subscription instance
  * @return service     String with the service label translated.
  */
  private String getServiceDescriptionFromSubscription(Subscription subscription) {
    String serviceDescription = "";
    ServiceInstance service = null;
    try{
      service = subscription.getServiceInstances().get(0);
    }catch(Exception e){
      log.error("Error retrieving the service description code of the subscription {} with language {}", subscription.getCode(), lang);
    }
    return getServiceDescriptionTranslated(service);
  }

 /**
  * Return the service description in the language passed as argument.
  * In the version 9.0 of OC, the Service description will be translatable, but now it isn't.
  * We use a CustomField to save the label in catalan and in spanish and use the language passed
  * as argument to return the correct label located.
  *
  * @param  subscription Subscription instance
  * @return service     String with the service label translated.
  */
  private String getServiceDescriptionTranslated(ServiceInstance service) {
    String serviceDescription = "";
    String service_lang_label = getCFServiceTranslatedLabel();
    try{
      serviceDescription = service.getServiceTemplate().getCfValues().getValuesByCode().get(service_lang_label).get(0).getStringValue();
    } catch (Exception e){
      log.error("Error retrieving the service description code of the service {} with language {}", service.getCode(), lang);
    }
    return serviceDescription;
  }

 /**
  * Return a string with the label of the Service description translation.
  *
  * @return service_lang_label  String with the service label to get the correct translation from the CFs of Service.
  */
  private String getCFServiceTranslatedLabel() {
    String service_lang_label = "CF_LANGUAGE_ES";
    if (lang.equals("CAT")) {
      service_lang_label = "CF_LANGUAGE_CAT";
    }
    return service_lang_label;
  }

  private String extractServiceFromSubscriptionAndEDR(Element consumption, List<Subscription> subscriptions) {
    Element edr = (Element) consumption.getElementsByTagName("edr").item(0);
    String subscriptionCode = edr.getAttribute("subscription");
    for (Subscription sub : subscriptions) {
      if (sub.getDescription() == null) {
        return "";
      }
      if (sub.getDescription().equals(subscriptionCode)) {
        return getServiceDescriptionFromSubscription(sub);
      }
    }
    return "";
  }

  private List<Element> getSubscriptionsFromDocument(Document document) {
    return getLinesBySubCategory(document, "SUBSCRIPTION");
  }

  private List<Element> getConsumptionsFromDocument(Document document) {
    return getLinesBySubCategory(document, "CONSUMPTION");
  }

  private List<Element> getDataConsumptionsFromDocument(Document document) {
    return getLinesBySubCategory(document, "CONSUMPTION_DATA");
  }

 /**
  * Get subCategories elements of the document and return a list with these elements.
  *
  * @param document Document of the invoice.
  * @param subCat   String with the subCategory code to search the elements.
  * @return line    List of Element instances inside the subCategory.
  */
  private List<Element> getLinesBySubCategory(Document document, String subCat) {
    List<Element> lines = new ArrayList<>();

    Element detailTag = (Element) document.getElementsByTagName("detail").item(0);
    int totalSubCategories = getSubCategoriesLength(detailTag);
    for (int i=0; i < totalSubCategories; i++) {
      Element subCategoryTag = (Element) detailTag.getElementsByTagName("subCategory").item(i);
      if (subCategoryTag.getAttribute("code").contains(subCat)) {
        NodeList nodeLines = (NodeList) subCategoryTag.getElementsByTagName("line");
        int lenghtNodeLines = nodeLines.getLength();
        for (int x=0; x < lenghtNodeLines; x++) {
          lines.add((Element) nodeLines.item(x));
        }
      }
    }
    return lines;
  }

  private int getSubCategoriesLength(Element detail){
    int totalSubCategories = 0;
    try{
      totalSubCategories = detail.getElementsByTagName("subCategory").getLength();
    } catch (NullPointerException e){}
    return totalSubCategories;
  }

 /**
  * Add iban tag to the XML and add the IBAN number as argument
  *
  * @param ca       CustomerAccount instances of the invoice
  * @param document Document of the invoice
  */
  private void addIban(CustomerAccount ca, Document document) {
    Element invoiceTag = (Element)document.getElementsByTagName("invoice").item(0);
    Element ibanTag = document.createElement("iban");
    ibanTag.setAttribute("number", getIban(ca));
    invoiceTag.appendChild(ibanTag);
  }

 /**
  * Extract the IBAN from the CustomerAccount. The PaymentMethod need be cast to DDPaymentMEthod.
  * If this cast fails, return an empty string and log as warning the error catched.
  *
  * @param ca       CustomerAccount instances of the invoice
  * @param document Document of the invoice
  */
  private String getIban(CustomerAccount ca) {
    PaymentMethod paymentMethod = ca.getPreferredPaymentMethod();
    paymentMethod = PersistenceUtils.initializeAndUnproxy(paymentMethod);
    String iban = "";
    if (paymentMethod != null) {
      if (paymentMethod.getPaymentType() == PaymentMethodEnum.DIRECTDEBIT) {
        if (((DDPaymentMethod) paymentMethod).getBankCoordinates() != null) {
          iban = ((DDPaymentMethod) paymentMethod).getBankCoordinates().getIban();
        }
      }
    }
    return iban;
  }

  private void addInvoiceLinesForCommercial(Invoice invoice, Element invoiceElement, Document document){
    Element invoiceLines = document.createElement("invoiceLines");
    getWalletOperationList();
    addSubscriptionLinesToInvoiceLinesList(invoice, invoiceLines, document);
    addConsumptionLinesToInvoiceLinesList(invoiceLines, document);
    addOneShotLinesToInvoiceLinesList(invoiceLines, document);
    invoiceElement.appendChild(invoiceLines);
  }

  private void addInvoiceLinesForAssociate(Invoice invoice, Element invoiceElement, Document document){
    Element invoiceLines = document.createElement("invoiceLines");
    for (InvoiceAgregate agregateInvoice : invoice.getInvoiceAgregates()){
      if (agregateInvoice.getAmountWithoutTax().compareTo(new BigDecimal(0)) == 0){
        continue;
      }
      Element line = document.createElement("line");
      line.setAttribute("Description", agregateInvoice.getDescription());
      line.setAttribute("PrDescription", agregateInvoice.getPrDescription());
      line.setAttribute("BillingAccount", agregateInvoice.getBillingAccount().toString());
      line.setAttribute("ItemNumber", String.valueOf(agregateInvoice.getItemNumber()));
      line.setAttribute("Amount", round(agregateInvoice.getAmount(), DEFAULT_SCALE_VALUE));
      line.setAttribute("AmountWithoutTax", round(agregateInvoice.getAmountWithoutTax(), DEFAULT_SCALE_VALUE));
      line.setAttribute("AmountTax", round(agregateInvoice.getAmountTax(), DEFAULT_SCALE_VALUE));
      line.setAttribute("AmountWithTax", round(agregateInvoice.getAmountWithTax(), DEFAULT_SCALE_VALUE));
      line.setAttribute("Invoice", agregateInvoice.getInvoice().toString());
      invoiceLines.appendChild(line);
    }
    invoiceElement.appendChild(invoiceLines);
  }
}

class ConsumptionInvoiceLine {
  private BigDecimal amount = new BigDecimal(0);
  private String tax;

  public BigDecimal getAmount() {
      return this.amount;
  }

  public String getTax() {
      return this.tax;
  }

  public void addAmount(BigDecimal num) {
    this.amount = this.amount.add(num);
  }

  public void setTax(String tax) {
    this.tax = tax;
  }
}
