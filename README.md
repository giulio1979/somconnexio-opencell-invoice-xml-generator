# OpenCell Invoice XML Generator

Som Connexió use OpenCell as billing tool. OpenCell use Jasper to generate the PDF invoices and to generate this PDFs Jasper need a valid template and a XML with the dataset to fill the template.

This repository manages the custom JAVA "script" used by OpenCell to generate the XML dataset.

Also we use a Postman Collection to manage the OpenCell configuration and we need report to the configuration the modifications in this JAVA file.

## What do this generator?

This generator add to the default XML dataset generated by OpenCell the next elements:

* Add the IBAN element
* Create invoice line elements
  - Commercial Invoice Lines
  - Adjustment Invoice Lines
  - Associated Invoice Lines
* Modify the consumptions elements
  - Add service description
  - Add MSIDSN
* Group data consumptions by day
* Remove the Subscriptions Categories elements

## How deploy your changes in production?

As we say previously, we use a Postman collection to configure the OpenCell environment. To add this piece of code in a JSON we need to scape it.
We use a *script* to scape the JAVA code.
Run:

```
$ python tools/scape_java.py
```
And a file will be created as `scapped_XML_generator` with the JAVA code scapped.

Then, follor the steps documented in the [Configuration wiki](https://gitlab.com/coopdevs/somconnexio-opencell-configuration/-/wikis/Fer-un-canvi-al-Postam-Collection)
